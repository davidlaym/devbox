# Mi Dev Box con Vagrant/virtualbox

Esta es la configuración de servicios para mi máquina virual de desarrollo base
donde tengo la mayoría de los servicios necesarios  para desarrollar aplicaciones
web, como mysql y redis. Esto me permite no tener instalados estos servicios
en mi máquina local y además me provee una consistencia en confifuración para 
mis distintos desarrollos.

## Sistema base

- OS: Ubuntu Server 16.04
- IP: 192.168.33.20

## Servicios instalados

### Mysql

- Puerto (mapeado): 3306
- Acceso remoto habilitado
- Root password: vagrant (acceso remoto habilitado)
- non-root access: vagrant/vagrant (all priviledges, acceso remoto habilitado)

### Redis

- Puerto (mapeado): 6379
- Protected mode: ON
- Acceso remoto habilitado
- Password: vagrant