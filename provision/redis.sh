#!/usr/bin/env bash

echo "############## Installing redis..."

# from https://www.digitalocean.com/community/tutorials/how-to-install-and-use-redis
curl -sSL http://download.redis.io/releases/redis-stable.tar.gz -o /tmp/redis.tar.gz
mkdir -p /tmp/redis
tar -xzf /tmp/redis.tar.gz -C /tmp/redis --strip-components=1
make -C /tmp/redis
make -C /tmp/redis install
echo -n | /tmp/redis/utils/install_server.sh
rm -rf /tmp/redis*

# See: http://redis.io/topics/faq
sysctl vm.overcommit_memory=1

# activating password protection 
sed -i -E 's/# requirepass .*/requirepass vagrant/g' /etc/redis/6379.conf

# deactivating ip binding (listen to every ip)
sed -i -E 's/(bind .*)/# bind \1/g' /etc/redis/6379.conf

service redis_6379 restart
