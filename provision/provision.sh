#!/usr/bin/env bash

HOSTNAME=devbox.vagrant

if [ -e "/etc/vagrant-provisioned" ];
then
    echo "Vagrant provisioning already completed. Skipping..."
    exit 0
else
    echo "Starting Vagrant provisioning process..."
fi

# Change the hostname so we can easily identify what environment we're on:
echo $HOSTNAME > /etc/hostname
# Update /etc/hosts to match new hostname to avoid "Unable to resolve hostname" issue:
echo "127.0.0.1 $HOSTNAME" >> /etc/hosts
# Use hostname command so that the new hostname takes effect immediately without a restart:
hostname $HOSTNAME

# Install core components
/vagrant/provision/core.sh

# Install Mysql
/vagrant/provision/mysql.sh

# Install Redis
/vagrant/provision/redis.sh

touch /etc/vagrant-provisioned

echo "--------------------------------------------------"
echo "Your vagrant instance is running at: http://$HOSTNAME/"
