#!/usr/bin/env bash

echo "############## Installing mysql..."

# install mysql with specified root password
debconf-set-selections <<< "mysql-server mysql-server/root_password password vagrant"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password vagrant"
apt-get -y install mysql-server mysql-client

# update mysql configuration to listen on any ip
sed -i -E "s/(bind-address)/#\1/" /etc/mysql/mysql.conf.d/mysqld.cnf

# grants root user with external access
mysql -uroot -pvagrant -e "use mysql; update user set host='%' where user='root';"

# creates named user 
mysql -uroot -pvagrant -e "CREATE USER 'vagrant'@'%' IDENTIFIED BY 'vagrant';"
mysql -uroot -pvagrant -e "GRANT ALL PRIVILEGES ON * . * TO 'vagrant'@'%';"

# refresh user priviledges
mysql -uroot -pvagrant -e "FLUSH PRIVILEGES;"

# restart mysql
sudo service mysql restart