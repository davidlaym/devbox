#!/usr/bin/env bash

echo "############## updating apt package list..."
apt-get -qq update

echo "############## upgrading packages..."
apt-get upgrade -y

echo "############## Installing build tools and core utils..."
apt-get install -y build-essential make gcc g++ git curl vim libcairo2-dev libav-tools nfs-common portmap tcl8.5
